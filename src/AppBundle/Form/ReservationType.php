<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Hotel;

class ReservationType extends AbstractType
{

    private $hotel;

    function __construct(Hotel $hotel) {
        $this->hotel = $hotel;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('checkinDate', 'date', ['label' => 'Check In Date', 'widget' => 'single_text', 'format' => 'dd-MM-yyyy',    'attr' => [
              'class' => 'form-control input-inline datepicker',
              'data-provide' => 'datepicker',
              'data-date-format' => 'dd-mm-yyyy',
              'data-date-start-date' => date('d-m-Y'),
              'data-date-autoclose' => true]])
            ->add('checkoutDate', 'date', ['label' => 'Check Out Date', 'widget' => 'single_text', 'format' => 'dd-MM-yyyy',    'attr' => [
              'class' => 'form-control input-inline datepicker',
              'data-provide' => 'datepicker',
              'data-date-format' => 'dd-mm-yyyy',
              'data-date-start-date' => date('d-m-Y'),
              'data-date-autoclose' => true]])
            ->add('customer', null, array('label' => 'Customer', 'required' => true, 'attr' => ['class' => "form-control"]))
            ->add('room', EntityType::class, array(
              'class' => 'AppBundle:Room',
              'choices' => $this->hotel->getRooms(),
              'label' => 'Room',
              'attr' => ['class' => "form-control"]
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Reservation'
        ));
    }
}
