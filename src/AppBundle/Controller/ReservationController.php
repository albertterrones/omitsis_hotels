<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Reservation;
use AppBundle\Form\ReservationType;
use Symfony\Component\Form\FormError;

/**
 * Reservation controller.
 */
class ReservationController extends Controller
{

    const INDEX  = 'reservation_index';
    const VIEW   = 'reservation_view';
    const CREATE = 'reservation_create';
    const UPDATE = 'reservation_update';
    const DELETE = 'reservation_delete';

     /**
      * Lists all Reservation entities for a given Hotel
      *
      * @Route("/hotel/{hotelId}/reservation", name="reservation_index")
      * @Method("GET")
      */
    public function indexAction($hotelId)
    {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('AppBundle:Reservation')->createQueryBuilder('re')
            ->leftJoin('AppBundle:Room', 'ro', 'WITH', 'ro.id = re.room')
            ->orderBy('re.customer', 'ASC')
            ->where('ro.hotel = :hotel')
            ->setParameter('hotel', $hotelId)
            ->getQuery();

        $reservations = $queryBuilder->getResult();
        $hotel = $em->getRepository('AppBundle:Hotel')->find($hotelId);
        if (!$hotel) {
            throw new NotFoundHttpException();
        }

        return $this->render('AppBundle:Reservation:index.html.twig', array(
            'reservations' => $reservations,
            'hotel' => $hotel
        ));
    }

    /**
     * Creates a new Reservation entity.
     *
     * @Route("/hotel/{hotelId}/reservation/new", name="reservation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $hotelId)
    {
        $reservation = new Reservation();
        $time = new \DateTime();
        $time->format('H:i:s \O\n Y-m-d');
        $reservation->setCheckinDate($time);
        $reservation->setCheckoutDate($time);

        $em = $this->getDoctrine()->getManager();
        $hotel = $em->getRepository('AppBundle:Hotel')->find($hotelId);
        if (!$hotel) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(new ReservationType($hotel), $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->checkAvailability($reservation, $form, $em)) {
                $em->persist($reservation);
                $em->flush();
                return $this->redirectToRoute(self::INDEX, array('hotelId' => $hotelId));
            }
        }

        $hotel = $em->getRepository('AppBundle:Hotel')->find($hotelId);
        if (!$hotel) {
            throw new NotFoundHttpException();
        }

        return $this->render('AppBundle:Reservation:new.html.twig', array(
            'reservation' => $reservation,
            'hotel' => $hotel,
            'form' => $form->createView(),
        ));
    }

    private function checkAvailability(Reservation $reservation, $form, $em)
    {
        if ($reservation->getCustomer() == null) {
          $form->get('customer')->addError(new FormError('A customer must be supplied in order to make a Reservation.'));
          return false;
        }
        if ($reservation->getCheckinDate() <= $reservation->getCheckoutDate() ) {
            $queryBuilder = $em->getRepository('AppBundle:Reservation')->createQueryBuilder('re')
                ->select('count(re.id)')
                -> where('
                    ((re.checkinDate BETWEEN :proposedCheckin AND :proposedCheckout)  AND (re.checkoutDate BETWEEN :proposedCheckin AND :proposedCheckout))
                    OR
                    ((:proposedCheckin BETWEEN re.checkinDate AND re.checkoutDate) OR (:proposedCheckout BETWEEN re.checkinDate AND re.checkoutDate))
                ')
                ->andWhere('re.room = :room')
                ->andWhere('re.id != :reservationId')
                ->setParameter('proposedCheckin', $reservation->getCheckinDate())
                ->setParameter('proposedCheckout', $reservation->getCheckoutDate())
                ->setParameter('room', $reservation->getRoom())
                ->setParameter('reservationId', ($reservation->getId() == null) ? -1 : $reservation->getId() )
                ->getQuery();
            $count = $queryBuilder->getSingleScalarResult();
            if ($count > 0){
                $form->get('room')->addError(new FormError('This room already contains a reservation for that period.'));
                return false;
            }
        } else {
          $form->get('checkoutDate')->addError(new FormError('Check Out Date must be greater than Check In Date.'));
          return false;
        }
        return true;
    }

    /**
     * Displays a form to edit an existing Reservation entity.
     *
     * @Route("hotel/{hotelId}/reservation/{id}/edit", name="reservation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Reservation $reservation, $hotelId)
    {
        $em = $this->getDoctrine()->getManager();
        $hotel = $em->getRepository('AppBundle:Hotel')->find($hotelId);
        if (!$hotel) {
            throw new NotFoundHttpException();
        }

        $editForm = $this->createForm(new ReservationType($hotel), $reservation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($this->checkAvailability($reservation, $editForm, $em)) {
                $em->persist($reservation);
                $em->flush();
                return $this->redirectToRoute(self::INDEX, array('hotelId' => $hotelId));
            }
        }

        return $this->render('AppBundle:Reservation:edit.html.twig', array(
            'reservation' => $reservation,
            'hotel' => $hotel,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Reservation entity.
     *
     * @Route("/{id}", name="reservation_delete")
     * @Route("/hotel/{hotelId}/reservation/{id}/delete", name="reservation_delete")
     * @Method({"GET","DELETE"})
     */
    public function deleteAction(Request $request, Reservation $reservation, $hotelId)
    {
        $form = $this->createDeleteForm($reservation);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $em->remove($reservation);
        $em->flush();

        return $this->redirectToRoute(self::INDEX, array('hotelId' => $hotelId));
    }

    /**
     * Creates a form to delete a Reservation entity.
     *
     * @param Reservation $reservation The Reservation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reservation $reservation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reservation_delete', array('hotelId' => $reservation->getRoom()->getHotel()->getId(), 'id' => $reservation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
