<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Hotel;
use AppBundle\Form\HotelType;

/**
 * Hotel controller.
 */
class HotelController extends Controller
{
  /**
   * Lists all Hotel entities.
   *
   * @Route("/hotel/", name="hotel_index")
   * @Method("GET")
   */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hotels = $em->getRepository('AppBundle:Hotel')->findAll();

        return $this->render('AppBundle:Hotel:index.html.twig', array(
            'hotels' => $hotels,
        ));
    }
}
