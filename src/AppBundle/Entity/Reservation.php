<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checkinDate", type="date")
     * @Assert\GreaterThanOrEqual("today")
     */
    private $checkinDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="checkoutDate", type="date")
     */
    private $checkoutDate;

    /**
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="reservations")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $room;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="reservations")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $customer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set checkinDate
     *
     * @param \DateTime $checkinDate
     * @return Reservation
     */
    public function setCheckinDate($checkinDate)
    {
        $this->checkinDate = $checkinDate;

        return $this;
    }

    /**
     * Get checkinDate
     *
     * @return \DateTime
     */
    public function getCheckinDate()
    {
        return $this->checkinDate;
    }

    /**
     * Set checkoutDate
     *
     * @param \DateTime $checkoutDate
     * @return Reservation
     */
    public function setCheckoutDate($checkoutDate)
    {
        $this->checkoutDate = $checkoutDate;

        return $this;
    }

    /**
     * Get checkoutDate
     *
     * @return \DateTime
     */
    public function getCheckoutDate()
    {
        return $this->checkoutDate;
    }

    /**
     * Set room
     *
     * @param Room $room
     * @return Reservation
     */
    public function setRoom(Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return Room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     * @return Reservation
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

}
