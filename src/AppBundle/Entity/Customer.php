<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="customer")
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $age;

    /**
    * @ORM\OneToMany(targetEntity="Reservation", mappedBy="customer")
    *
    */
    protected $reservations;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return Customer
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
    * Add reservation
    *
    * @param Reservation $reservation
    * @return Customer
    */
    public function addReservation(Reservation $reservation)
    {
      $this->reservations[] = $reservation;
      return $this;
    }

    /**
    * Remove reservation
    *
    * @param Reservation $reservations
    */
    public function removeReservation(Reservation $reservation)
    {
      $this->reservations->removeElement($reservation);
    }

    /**
    * Get reservations
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getReservations()
    {
      return $this->reservations;
    }

    public function __toString()
    {
       return (string)$this->name;
    }    

}
