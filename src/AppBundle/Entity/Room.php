<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="room")
 */
class Room
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $number;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="rooms")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    protected $hotel;

    /**
    * @ORM\OneToMany(targetEntity="Reservation", mappedBy="room")
    *
    */
    protected $reservations;

    public function __construct()
    {
      $this->reservations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Room
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set hotel
     *
     * @param Hotel $hotel
     * @return Room
     */
    public function setHotel(Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return Hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
    * Add reservation
    *
    * @param Reservation $reservation
    * @return Room
    */
    public function addReservation(Reservation $reservation)
    {
      $this->reservations[] = $reservation;
      return $this;
    }

    /**
    * Remove reservation
    *
    * @param Reservation $reservations
    */
    public function removeReservation(Reservation $reservation)
    {
      $this->reservations->removeElement($reservation);
    }

    /**
    * Get reservations
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getReservations()
    {
      return $this->reservations;
    }

    public function __toString()
    {
       return (string)$this->number;
    }

}
