<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="hotel")
 */
class Hotel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $stars;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $city;

    /**
    * @ORM\OneToMany(targetEntity="Room", mappedBy="hotel")
    *
    */
    protected $rooms;

    public function __construct()
    {
      $this->rooms = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Hotel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set stars
     *
     * @param integer $stars
     * @return Hotel
     */
    public function setStars($stars)
    {
        $this->stars = $stars;

        return $this;
    }

    /**
     * Get stars
     *
     * @return integer
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Hotel
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
    * Add room
    *
    * @param Room $room
    * @return Hotel
    */
    public function addRoom(Room $room)
    {
      $this->rooms[] = $room;
      return $this;
    }

    /**
    * Remove room
    *
    * @param Room $rooms
    */
    public function removeRoom(Room $room)
    {
      $this->rooms->removeElement($room);
    }

    /**
    * Get rooms
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getRooms()
    {
      return $this->rooms;
    }

    public function __toString()
    {
       return (string)$this->name;
    }

}
