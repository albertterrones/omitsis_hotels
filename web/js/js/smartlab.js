$(function () {
    var createForm = function (action, data) {
        var $form = $('<form action="' + action + '" method="POST"></form>');
        for (input in data) {
            if (data.hasOwnProperty(input)) {
                $form.append('<input name="' + input + '" value="' + data[input] + '">');
            }
        }
        return $form;
    };
    $(document).on('click', 'a[data-delete-method]', function (e) {
        e.preventDefault();
        var $this = $(this);
        var message = $this.attr('data-delete-confirm');
        if (message) {
            if (!confirm(message)) { return false; }
        }
        var $form = createForm($this.attr('href'), { _method: $this.attr('data-delete-method').toUpperCase() }).hide();
        $('body').append($form);
        $form.submit();
    });
});
